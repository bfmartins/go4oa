**The gO4OA is the operational version of the O4OA reference model grounded over gUFO and fulfilled with the cybersecurity study cases data.**

---

![o4oa-logo](https://bfmartins.gitlab.io/go4oa/assets/logo/go4oa.png){width=25%}

---

For more details, see the [reference ontology documentation](https://bfmartins.gitlab.io/o4oa);